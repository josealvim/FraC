# FraC is a simple C fractal renderer

## To install
 * Download or clone this repository.
 * Go into the it's root folder.
 * Run `$ make` (you may have to install `make`) in the command line.
 * The executable should be inside the newly created folder `build/bin` under the name of `FraC.out`.
 
## Using `FraC`
For a bland stock and badly cropped Mandelbrot-set picture, simply invoke the program!  
 `$ ./FraC.out` (assuming you're in the same folder as it is.
You can also run  
 `$ ./FraC.out --help`  
To learn more about its functionalities.  
  
You can:
 * Chose THREE incredible fractals.
 * Change the Center of the image.
 * Change the dimensions of the image.
 * Change ~gasp~ the output name of the file (It only exports .ppm format though).
 * And may other (not that many) features! Run `--help` to find out!

## Example 
`./build/bin/FraC.out -f burning-ship -c -1.75 0 -s 0.1 -i 8000 -d 1000 1000`


## Okay, now what?
I don't know. You can get your sexy pictures, `convert` them to `.png`so they won't weigh like 1.8Gb, throw them into GIMP and make it shine!