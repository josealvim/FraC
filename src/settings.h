#ifndef SETTINGS_H
#define SETTINGS_H

#include "jose/geometry.h"

typedef struct {
    char        *filename;
    unsigned int num_args;
    double      *args;
    unsigned int iterations;
    Int2d        dimension;
    double       range;
    double       zoom;
    Double2d     center;
    int (*fractal)(Double2d, void*);
} Settings;

#endif