#ifndef FRACTAL_FUNCTIONS_H
#define FRACTAL_FUNCTIONS_H

#include "jose/geometry.h"

int burning_ship(Double2d point, void *s);
int   mandelbrot(Double2d point, void *s);
int        julia(Double2d point, void *s);

#endif