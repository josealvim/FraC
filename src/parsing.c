
#include "stdio.h"
#include "stdlib.h"

#include "parsing.h"
#include "fractals.h"

#include "jose/string.h"
#include "jose/geometry.h"

int  parse_args(int argc, char *argv[], Settings *settings) {
    int i, which = 0;
    char *fractal_names[] = {"mandelbrot", "burning-ship", "julia", "\0"};

    settings->filename    = malloc(6*sizeof(char));
        if (!settings->filename) {
            printf("Could not alloc memory for default filename\n");
            exit(-1);
        }
       strset(settings->filename, "a.ppm");
    settings->iterations  = 1024;
    settings->center.x    = 0.0;
    settings->center.y    = 0.0;
    settings->dimension.x = 512;
    settings->dimension.y = 512;
    settings->range       = 3.0;
    settings->zoom        = 1.0;
    settings->fractal     = &mandelbrot;
    settings->args        = NULL;
    settings->num_args    = 0;

    for (i = 1; i < argc; i++ ) {
        if (streq("--help", argv[i])) {
            help( "Showing help Menu:" );
            return 0;
        } else
        if (streq("--list", argv[i])) {
            fractal_list();
            return 0;
        } else
        if (streq("-c", argv[i])) {
            i++; if (!(i<argc)) { 
                help("syntax error, on -c; expected 2 arguments, got 0:\n"); 
                return 0;
            }
            settings->center.x    = atof(argv[i]);
            
            i++; if (!(i<argc)) { 
                help("syntax error, on -c; expected 2 arguments, got 1:\n"); 
                return 0;
            }
            settings->center.y    = atof(argv[i]);
        } else
        if (streq("-d", argv[i])) {
            i++; if (!(i<argc)) { 
                help("syntax error, on -d; expected 2 arguments, got 0:\n"); 
                return 0;
            }
            settings->dimension.x = atoi(argv[i]);

            i++; if (!(i<argc)) { 
                help("syntax error, on -d; expected 2 arguments, got 1:\n"); 
                return 0;
            }
            settings->dimension.y = atoi(argv[i]);
        } else
        if (streq("-f", argv[i])) {
            i++; if (!(i<argc)) { 
                help("syntax error, on -f; expected 1 arguments, got 0:\n"); 
                return 0;
            }

            for (which = 0; fractal_names[which][0] && !streq(argv[i],fractal_names[which]); which++);
            if (!fractal_names[which][0]) {
                printf("error, on -f; No such fractal name registered \"%s\", try --list\n", argv[i]);
                return 0;
            }

            switch (which) {
              case 0:
                settings->fractal = &mandelbrot; break;
              case 1:
                settings->fractal = &burning_ship; break;
              case 2:
                settings->fractal = &julia; break;
            }
        } else
        if (streq("-i", argv[i])) {
            i++; if (!(i<argc)) { 
                help("syntax error, on -i; expected 1 argument, got 0:\n"); 
                return 0;
            }
            settings->iterations  = atoi(argv[i]);
        } else
        if (streq("-r", argv[i])) {
            i++; if (!(i<argc)) { 
                help("syntax error, on -r; expected 1 argument, got 0:\n"); 
                return 0;
            }
            settings->range      = atof(argv[i]);
        } else
        if (streq("-s", argv[i])) {
            i++; if (!(i<argc)) { 
                help("syntax error, on -r; expected 1 argument, got 0:\n"); 
                return 0;
            }
            settings->zoom       = atof(argv[i]);
        } else
        if (streq("-o", argv[i])) {
            i++; if (!(i<argc)) { 
                help("syntax error, on -o; expected 1 argument, got 0:\n"); 
                return 0;
            }
            int size;
            
            for (size = 0; argv[i][size]; size++);
            if (!size)
                return 0;
            free(settings->filename);
            settings->filename = malloc(size*sizeof(char));
                if (!settings->filename) {
                    printf("Could not alloc memory for filename\n");
                    exit(-1);
                }
                strset(settings->filename, argv[i]);
        } else
        if (streq("{", argv[i])) {
            int j = i, k = 0;
            
            while (j < argc && !streq("}", argv[j])) 
                j++;
            
            if (!(j<argc)) {
                help("syntax error, got '{', but no '}'\n"); 
                return 0;
            }
            i++;

            settings->num_args = j - i;
            settings->args     = malloc(sizeof(double)*(settings->num_args));
            while (k < settings->num_args) settings->args[k++] = atof(argv[i++]);
        }
    }

    printf("Initialized system with settings:\n");
    printf("  *     center: %f, %f\n", 
        settings->center.x,
        settings->center.y
    );
    printf("  *  dimension: %d, %d\n", 
        settings->dimension.x,
        settings->dimension.y
    );
    printf("  *    fractal: %s\n", 
        fractal_names[which]
    );
    printf("  * iterations: %d\n", 
        settings->iterations
    );
    printf("  * outputname: %s\n", 
        settings->filename
    );
    printf("  *      range: %f\n", 
        settings->range
    );
    printf("  *      scale: %f\n", 
        settings->zoom
    );

    return 1;
}

void help(char *s) {
    printf("%s\n", s);

    printf("  --help shows this dialog\n");
    printf("  --list shows all avaliable fractals and their descriptions\n");
    printf("   -c sets the center of the picture in doubles [ expects  two doubles ] \n");
    printf("   -d sets the size of the picture   in pixels  [ expects  two doubles ] \n");
    printf("   -f sets which fractal to render for the pic  [    fractal   name    ] \n");
    printf("   -i sets the number of iterations per point   [ expects  an uinteger ] \n");
    printf("   -o sets the filename of the output           [ expects <string>.ppm ] \n");
    printf("   -r sets the bailout range for the picture    [ expects   a   double ] \n");
    printf("   -s sets the scale for the picture            [ expects   a   double ] \n");
    printf("    { begins reading additional parameters\n");
    printf("    } ends   reading additional parameters\n");
}

void fractal_list() {
    printf("Currently we have the following fractals implemented:\n");
    printf("    -f mandelbrot  :Mandelbrot\n");
    printf("      doesn't expect any arguments, they'll be ignored\n\n");

    printf("    -f burning-ship: Burning ship\n");
    printf("      doesn't expect any arguments, they'll be ignored\n\n");
}