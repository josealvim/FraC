#ifndef ARGUMENT_PARSING_H
#define ARGUMENT_PARSING_H

#include "settings.h"

int  parse_args(int argc, char *argv[], Settings *settings);
void help(char *s);
void fractal_list();

#endif