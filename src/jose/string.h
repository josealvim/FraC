#ifndef JOSE_STRING_H
#define JOSE_STRING_H

/// Sets string `a` to string `b`, `a` must be big enough.
void strset(char *a, char *b);

/// Checks equality of strings
int   streq(char *a, char *b);

void strset(char *a, char *b) {
    for (; *b; b++) {
        *a=*b;
        a++;
    }
}

int  streq(char *a, char *b) {
    char *i, *j;
    for (i =  a; *i; i++);
    for (j =  b; *j; j++);
    if  (i-a != j-b) {
        return 0;
    } 
    for (i = a, j = b;*i;i++, j++)
        if (*i != *j) return 0;
    return 1;
}

#endif