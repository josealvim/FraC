#ifndef JOSE_GEOMETRY_H
#define JOSE_GEOMETRY_H

typedef struct {
    int x;
    int y;
} Int2d;

typedef struct {
    float x;
    float y;
} Double2d;

#endif