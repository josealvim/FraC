
#include "stdio.h"
#include "stdlib.h"

#include "fractals.h"
#include "settings.h"

int burning_ship(Double2d point, void *s) {
    Settings *settings = (Settings*)s;

    int i;
    const int MAX_ITER       = settings->iterations; 
    const double bailout_sqr = settings->range*settings->range;
    Double2d p   = point;
    Double2d tmp = {0,0};
    
    for (i = 0; i < MAX_ITER && p.x*p.x+p.y*p.y < bailout_sqr; i++) {
        tmp = p;
        tmp.x = tmp.x>0?tmp.x:-tmp.x;
        tmp.y = tmp.y>0?tmp.y:-tmp.y;

        p.x = tmp.x*tmp.x-tmp.y*tmp.y + point.x;
        p.y = 2  *  tmp.x*tmp.y       + point.y;
    } 

    return i;
}

int mandelbrot(Double2d point, void *s) {
    Settings *settings = (Settings*)s;
    
    int i;
    const int MAX_ITER       = settings->iterations; 
    const double bailout_sqr = settings->range*settings->range;
    Double2d p   = point;
    Double2d tmp = {0,0};
   
    for (i = 0; (i < MAX_ITER) && (p.x*p.x+p.y*p.y < bailout_sqr); i++) {
        tmp = p;
        p.x = tmp.x*tmp.x-tmp.y*tmp.y + point.x;
        p.y = 2  *  tmp.x*tmp.y       + point.y;
    }

    return i;
}

int julia(Double2d point, void *s) {
    Settings *settings = (Settings*)s;
    
    int i;
    const int MAX_ITER       = settings->iterations; 
    const double bailout_sqr = settings->range*settings->range;
    Double2d p   = point;
    Double2d tmp = {0,0};

    if (settings->num_args < 2) {
        printf("Julia needs (at least) 2 arguments: a+bi\n");
        exit(-1);
    }

    const Double2d constant = {settings->args[0], settings->args[1]};
    
    for (i = 0; (i < MAX_ITER) && (p.x*p.x+p.y*p.y < bailout_sqr); i++) {
        tmp = p;
        p.x = tmp.x*tmp.x-tmp.y*tmp.y + constant.x;
        p.y = 2  *  tmp.x*tmp.y       + constant.y;
    }

    return i;
}