
#include "parsing.h"
#include "fractals.h"
#include "settings.h"

#include "jose/geometry.h"

#include "stdio.h"
#include "stdlib.h"

int  make_frac(Settings *settings);
void cleanup(Settings *settings);

int main(int argc, char *argv[]) {
    Settings settings;

    if (!parse_args(argc, argv, &settings))
        return -1;

    if (make_frac(&settings))
        printf("Program ended successfully\n");
    else
        printf("Program ended badly, errors on make_frac\n");

    cleanup(&settings);
    return 0;
}

int make_frac(Settings *settings) {
    unsigned short value;
    unsigned int i, j;
    const unsigned H = settings->dimension.y, 
                   W = settings->dimension.x;
    FILE *image_p;
    float scale;
    const double ZOOM     = settings->zoom,
                 CENTER_X = settings->center.x,
                 CENTER_Y = settings->center.y;

    image_p = fopen(settings->filename, "w");
    if (!image_p) {
        printf("Could not open '%s' with write permition\n", settings->filename);
        return 0;
    }
    printf("\n");

    fprintf(image_p,
        "P3 %d %d %d\n", 
        W, 
        H, 
        settings->iterations);

    printf("| 0%%");
    for (int counter = 5; counter < 100; counter++)
        printf(" ");
    printf("| 100%%\n");

    int progress = 0;
    for (i = 0; i < H; i++) {
    for (j = 0; j < W; j++) {
        if ( ( 100.0 * i * W + j ) / ( W * H ) > progress ) {
            progress++; printf("*");
            fflush(stdout);
        }

        Double2d point;
        point.y = CENTER_Y + 2 * ZOOM * (i / (double) H - 0.5);
        point.x = CENTER_X + 2 * ZOOM * (j / (double) W - 0.5);

        value = ( * (settings->fractal) )(point, settings);
        fprintf(image_p,"%d %d %d\n", value, value, value);
    }}
    printf("\n");

    fclose(image_p);
    return 1;
}

void cleanup(Settings *settings) {
    free(settings->filename);
    free(settings->args);
}

