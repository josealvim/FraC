FraC: ready main.c.o fractals.c.o parsing.c.o
	@echo "Linking FraC"
	@gcc build/lib/main.c.o build/lib/fractals.c.o build/lib/parsing.c.o -O7 -o build/bin/FraC.out -g

ready:
	@rm -rf build
	@mkdir build 
	@mkdir build/lib
	@mkdir build/bin
	@echo "Directory structure ready. May begin compiling\n"

main.c.o:
	@echo Building main.c.o
	@gcc src/main.c -c -O7 -o build/lib/main.c.o -g

fractals.c.o:
	@echo Building fractals.c.o
	@gcc src/fractals.c -c -O7 -o build/lib/fractals.c.o -g

parsing.c.o:
	@echo Building parsing.c.o
	@gcc src/parsing.c -c -O7 -o build/lib/parsing.c.o -g